<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>GNU Marcador 2v</title>
	<script type="text/javascript" src="jquery-1.11.1.min.js"></script>

</head>
<script type="text/javascript">

$( document ).ready(function() {

	var data = {
		e: '<?=$_REQUEST["e"]?>',
		m: '<?=$_REQUEST["m"]?>'
	};

	var he_datos = [];

	var funConsulta = function(){

		$.ajax({
			url: "wta_consulta.php",
			dataType: "json",
			data: data,
			beforeSend: function( xhr ) {
				xhr.overrideMimeType("text/plain; charset=x-user-defined");
			}
		}).done(function( data ) {

			if (data.sys_recargar == 1) {
				location.reload();
			}

			if (data.match_state == "P" || data.match_state == "E") {
				$("#wta_configurando").hide();

			} else {
				$("#wta_configurando").show();
			}

			if(data.Server == 0) {

				document.getElementById("saqueEquipoUno").style.backgroundColor = "#FD6B0D";
				document.getElementById("saqueEquipoDos").style.backgroundColor = "#1C3766";

			} else if(data.Server == 1) {
				document.getElementById("saqueEquipoUno").style.backgroundColor = "#1C3766";
				document.getElementById("saqueEquipoDos").style.backgroundColor = "#FD6B0D";

			} else if(data.Server == 2) {
				document.getElementById("saqueEquipoUno").style.backgroundColor = "#FD6B0D";
				document.getElementById("saqueEquipoDos").style.backgroundColor = "#1C3766";
			} else if(data.Server == 3) {
				document.getElementById("saqueEquipoUno").style.backgroundColor = "#1C3766";
				document.getElementById("saqueEquipoDos").style.backgroundColor = "#FD6B0D";
			}

			$.each(data, function(key, valor) {
				if( $("*#" + key).length )
					if (key == 'country1' || key == 'country2' || key == 'country1a' || key == 'country2a') {
						if (valor.length > 0)
						$("*#" + key).html('(' + valor + ')');
					}else
						$("*#" + key).html(valor);

			});
		});
	};

	funConsulta();
	setInterval(function(){
		funConsulta();
	}, 2000);
});
</script>

<?php
	$country_code = 'mx';
	$img_ban = 'https://flagpedia.net/data/flags/normal/' + $country_code + '.png';
?>
<style type="text/css">
	
/*Diseño general */
*{
	margin: 0px;
	padding: 0px;
}

:root{
	--main-tamaño-ancho-pantalla:384px;
	--main-tamaño-alto-pantalla:384px;
	--main-tamaño-txt-equipos:55px;
	--main-tamaño-txt-numeros:60px;
}

@font-face{
	font-family: marcador;
	src: url("font/ProximaNova-Bold.otf");
}

body{
	font-family: marcador;	
	color: #FFF;
	background-color: #000;
	overflow:hidden;
}

/*Tamaño de las secciones */

#general{
	margin: auto;
	width: var(--main-tamaño-ancho-pantalla);
	height: var(--main-tamaño-alto-pantalla);
	background-color: #FFF;
	float: left;

}

#tabla{
	width: 98%;
	margin-left: 1%;
	margin-right: 1%;
	height: 40%;
	background-color: #FFF;
}

#equipo1{
	width: 100%;
	height: 30%;
	background-color: #1C3766;

}

#equipo2{
	width: 100%;
	height: 30%;
	background-color: #1C3766;
}

/*Distribucion del tamaño del texto de los jugadores*/

#equipo1 p{
	width: 100%;
	height: 50%;
}

#equipo2 p{
	width: 100%;
	height: 50%;
}
.nombres{
		font-size: var(--main-tamaño-txt-equipos);
		margin-left: 9px;
	}

/*Propiedades de imagen del logo*/

#imagen{
	width: 100%;
	display: block;
  	margin-left: auto;
  	margin-right: auto;
}

/*Propiedadesd de la tabla*/

.table{
	width: 100%;
	height: 100%;
}

.table, th, td {
    font-size: var(--main-tamaño-txt-numeros);
	border-spacing: 0px;
	margin: 0px;
	padding: none;
    border: 4px solid white;
    /*border-color: #000;*/
    background-color: #1C3766;
    border-collapse: collapse;
}

/*GUARDA PANTALLA*/

#wta_configurando {
 /* z-index: 100;*/
  width: var(--main-tamaño-ancho-pantalla);
  height: var(--main-tamaño-alto-pantalla);
  background-color: #000;
  position: absolute;
  top: 0px;
  left: 0px;
  /*font-family: "TodaySHOP";
  font-size: 20px;*/
  color: #fff;
  text-align: center;
  vertical-align: center;
  opacity:0.8;
}

#wta_configurando div {
  padding-top: 120px;
}

#gnp{
	width: 100px;
	height: 100px;
}

#wta{
	padding-top: 5px;
	width: 100px;
	height: 28px;
	margin-left: 0px;
	padding-bottom: 20px;
}
#sombra{
 /* z-index: 100;*/
  width: var(--main-tamaño-ancho-pantalla);
  height: var(--main-tamaño-alto-pantalla);
  margin-left:var(--main-tamaño-ancho-pantalla);
  background-color: #000;
  position: absolute;
  top: 0px;
  left: 0px;
  /*font-family: "TodaySHOP";
  font-size: 20px;*/
  color: #fff;
  text-align: center;
  vertical-align: center;
  opacity:1;
}
</style>
<body>
	<div id="wta_configurando">
	<div>
		<img id="gnp" src="img/gnp.png"><br>
		<img id="wta" src="img/atp.png">
	</div>
	</div>

	<div id="general">

		<div id="equipo1">
			<p id="surname1a" class="nombres">sjfñlsjdlkfjdslkf</p>
			<p id="surname1" class="nombres">l.sdjfjdlkfjñkdsljfñlkds</p>
		</div>

		<div id="tabla">
						<table class="table">
				<tr>
					<th style="width: 25%; background-color: #FD6B0D;" id="saqueEquipoUno"><p id="game1">40</p></th>
					<th style="background-color: #FFF" id="ii" style="width: 3%"></th>
					<th style="width: 25%;"><p id="Set1_1">0</p></th>
					<th style="width: 25%;"><p id="Set2_1">0</p></th>
					<th style="width: 25%;"><p id="Set3_1">0</p></th>
				</tr>
				<tr>
					<th style="background-color: #FD6B0D;" id="saqueEquipoDos"><p id="game2">15</p></th>
					<th style="background-color: #FFF" id="i"></th>
					<th><p id="Set1_2"></p>0</th>
					<th><p id="Set2_2"></p>0</th>
					<th><p id="Set3_2"></p>0</th>

				</tr>
			</table>
		</div>

		<div id="equipo2">
			<p id="surname2" class="nombres">iwehruidfjejf</p>
			<p id="surname2a" class="nombres">hfkdsfjdsiojfdsjf</p>
		</div>
	</div>
	<div id="sombra"></div>
</body>
</html>