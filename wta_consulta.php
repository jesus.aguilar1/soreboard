﻿<?php
include("conexion.php");

$wta_variables = "wta_variables";
$wta_variables = fopen($wta_variables, "r");
$wta_resultado = array(1);
$resultadoPkc = DbModulo_General::SelectPackage($_REQUEST["e"], $_REQUEST["m"]);

if($resultadoPkc[0])
{
    $PacketData	= $resultadoPkc[0]['PacketData'];
    $PacketDataXml = simplexml_load_string($PacketData);
    $PacketDataXml_Packet = base64_decode($PacketDataXml -> Packet);
    
    while(!feof($wta_variables)){
		$wta_linea = fgets($wta_variables);
		$wta_array = explode("|", $wta_linea);

		$wta_packets_value = $PacketDataXml_Packet;
		$wta_array[0] = $wta_array[0] - 1;

		$wta_resultado[trim($wta_array[2])] = trim(substr($wta_packets_value, $wta_array[0], $wta_array[1]));
    }
}

$wta_resultado['sys_recargar'] = 0;

echo json_encode($wta_resultado);
?>