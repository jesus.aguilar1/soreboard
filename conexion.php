<?php
class DatabaseSQL {

  private $g_conexion = null;
  private $g_instancia = "abiertosrv";
  private $g_user = "sa";
  private $g_pass = "Nra9maar.";
  private $g_database = "Scoreboard";
  private $g_charset = 'UTF-8';

  public function __construct(){
    $l_datos = array(
        'Database' => $this->g_database,
        'PWD' => $this->g_pass,
        'UID' => $this->g_user,
        'CharacterSet' => $this->g_charset
    );

    $this->g_conexion = sqlsrv_connect($this->g_instancia, $l_datos);
  }
  public function __construct0($p_instancia, $p_user, $p_pass, $p_database, $p_charset = 'UTF-8') {
      $l_datos = array(
          'Database' => $p_database,
          'PWD' => $p_pass,
          'UID' => $p_user,
          'CharacterSet' => $p_charset
      );

      $this->g_conexion = sqlsrv_connect($p_instancia, $l_datos);
  }
  public function __destruct() {
      if (!is_null($this->g_conexion)) {
          sqlsrv_close($this->g_conexion);
      }
  }
  public function ejecutarConsulta($p_comando, $p_parametros = null) {
      $l_filas = array();
      $l_resultado = sqlsrv_query($this->g_conexion, $p_comando, $p_parametros, array('Scrollable' => SQLSRV_CURSOR_STATIC));
      while ($r = sqlsrv_fetch_array($l_resultado)) {
          $l_filas[] = $r;
      }
      sqlsrv_free_stmt($l_resultado);

      return $l_filas;
  }
  public function ejecutarComando($p_comando, $p_parametros = null) {
      sqlsrv_query($this->g_conexion, $p_comando, $p_parametros, null);
  }
}

class DbModulo_General
{
  public static function SelectPackage($EventId, $CourtId){
    $l_base = new DatabaseSQL();
    $l_comando = "SELECT TOP(1) * FROM Packet WHERE EventID = ? AND CourtID = ? ORDER BY ID DESC";
    $l_parametros = array($EventId, $CourtId);
    $l_resultado = $l_base->ejecutarConsulta($l_comando, $l_parametros);
    return $l_resultado;
  }
}
?>
